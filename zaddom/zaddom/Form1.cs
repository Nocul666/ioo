﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace zaddom
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            but1 b = new but1();
            b.wow();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            but2 a = new but2();
            a.nani();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            wyl w = new wyl();
            w.wylo();
        }
    }
    class wyl
    {
        public void wylo()
        {
            Application.Exit();
        }
    }

    class but1
    {
        public void wow()
        {
            SoundPlayer splayer = new SoundPlayer("1.wav");
            splayer.Play();

        }
    }

    class but2
    {
        public void nani()
        {
            SoundPlayer splayer1 = new SoundPlayer("2.wav");
            splayer1.Play();

        }
    }
}
